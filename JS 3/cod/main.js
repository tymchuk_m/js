const styles = ["Джаз", "Блюз"]
document.write("<p> Початкова форма:" + "<br/>" + styles + "<br/>")

styles.push("Рок-н-ролл")
document.write("<p> Після добавлення:" + "<br/>" + styles)

const styleMid = styles[Math.floor(styles.length / 2)] = ("Класика");
document.write("<p> Замінено:" + "<br/>" + styles);

const styleDelete = styles.shift();
document.write("<p> Видалений стиль:" + "<br/>" + styleDelete);

const stylesAdd = styles.unshift("Рэп", "Регги");
document.write("<p> Завершена форма:" + "<br/>" + styles);